import spacy
import wmd
import credentials
import pandas as pd
import itertools
from itertools import product
import psycopg2
#import re
#from nltk.corpus import stopwords
#from collections import defaultdict

def insert_sql(qry, conn, columns):
    try:
        cur.execute(qry, (columns))
        #print(cur.mogrify(qry, (columns))) #print query
        print('mysql commit')
    except psycopg2.IntegrityError as Exception:
        conn.rollback()
        print(Exception)
    else:
        conn.commit()

##clean sentance
#stop_words = stopwords.words('english')
#regex = re.compile('[^A-Za-z ]') #remove all non-alphabet chars from string
#def process(doc):
#    try:
#        doc = regex.sub('', doc) #remove all non-alphabet chars from string 
#        doc = doc.lower()  # Lower the text.
#        doc = doc.split()
#        doc = [w for w in doc if w.isalpha()] 
#        doc = [word for word in doc if word not in stop_words]
#        doc = ' '.join([line for line in doc])
#    except:
#        pass
#        print('error: ', doc)
#    return doc


#stop_words = stopwords.words('english')
#regex = re.compile('[^A-Za-z ]') #remove all non-alphabet chars from string
def process(doc):
    try:
        #doc = regex.sub('', doc) #remove all non-alphabet chars from string 
        doc = doc.lower()  # Lower the text.
        doc = doc.replace('shop houzz:', '').replace('guest picks:', '').replace('highest-rated', '').replace('up to', '').replace('must-haves', '').replace('every style', '').replace('bestselling', '')
        doc = doc.split()
        #doc = [word for word in doc if word not in stop_words]
        doc = ' '.join([line for line in doc])
    except:
        pass
        print('error: ', doc)
    return doc
    
    
#connect to postgresql
try:
    login = "dbname=%s user=%s host=%s password=%s" % (credentials.login['dbname'],credentials.login['user'],credentials.login['host'],credentials.login['password'])
    postgresqlconn = psycopg2.connect(login)
except Exception as e:
    print("cannot connect to Google postgresSQL")
    print(e)
cur = postgresqlconn.cursor()

ideabook_descriptions = pd.read_csv('index_ideabooks_2017.csv')
ideabook_descriptions.dropna()

nlp = spacy.load('en_core_web_lg', create_pipeline=wmd.WMD.create_spacy_pipeline)
#nlp = spacy.load('en', create_pipeline=wmd.WMD.create_spacy_pipeline)

title_list = []
gallery_id_dict = {}
gallery_title_dict = {}


for i, (index, row) in enumerate(ideabook_descriptions.iterrows()):
    gallery_id = row['gallery_id']
    title = row['title']
    gallery_id_dict[i] = [gallery_id]
    gallery_title_dict[i] = [title]


for a, b in list(map(dict, itertools.combinations(gallery_title_dict.items(), 2))):
    doc1 = nlp(str(process(gallery_title_dict[a][0])))
    #doc1 = nlp(str(gallery_title_dict[a][0]))
    doc2 = nlp(str(process(gallery_title_dict[b][0])))
    #doc2 = nlp(str(gallery_title_dict[b][0]))
    sim_per = (doc1.similarity(doc2))

    gallery_id_site_id1 = gallery_id_dict[a][0]
    gallery_id_site_id2 = gallery_id_dict[b][0]

    if sim_per >= 0.90:
        print(process(gallery_title_dict[a][0]),' -- ',process(gallery_title_dict[b][0]),sim_per)
        try:
            qry = "INSERT INTO ideabook_shop_results (gallery_id1, title_1, gallery_id2, title_2, sim_per) VALUES (%s, %s, %s, %s, %s)"
            columns = (gallery_id_site_id1, gallery_title_dict[a][0], gallery_id_site_id2, gallery_title_dict[b][0], sim_per)
            insert_sql(qry, postgresqlconn, columns)
        except:
            raise ValueError('testerror')