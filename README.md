---

## Word Movers Distance

A script to get the most similar documents for a list of titles.

---

## Overview

The script uses word mover's distance and [spaCy](https://spacy.io/) to find fimilar ideabooks without a dictionary of similar keywords.

Word Mover's Distance: WMD is a method that allows us to assess the "distance" between two documents in a meaningful way, even when they have no words in common. 

---

## Install requirements: 

pip install -r requirements.txt